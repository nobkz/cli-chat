var milkcocoa = new MilkCocoa("https://io-zhzjxdtrv.mlkcca.com");

function readMessages(){
    var dataStore = milkcocoa.dataStore("users");
    dataStore.query().limit(30).done(function(data) {
        console.log(data);
    });
}


function addAccount(email, password){
    milkcocoa.addAccount(email, password, {},function(err,data){
        console.log("addAccount"); 
        var userDataStore = milkcocoa.dataStore("users");
        userDataStore.push({ user_id : user.id  },function(err,data){
            console.log(err,data);
        });
    }); 
}


function login(email,password,successCallback, failedCallback){
    milkcocoa.login(email,password,function(err,user){
        if( err != null ){
            failedCallback(err);
        }else{
            successCallback(user);
        }
    });
}


function failedCallback(err){
    console.log(err);
}

function successCallback(user){
    var dataStore = milkcocoa.dataStore("user/" + user.id + "/messages");
    function message(text){
        dataStore.push({ message : text, email : user.email });
    };

    document.getElementById("push-message-btn").onclick = function(data){
        var text = document.getElementById("push-message-text").value;
        message(text); 
    };
}


window.onload = function(){
    readMessages();
    var signup_btn = document.getElementById("sign-up-btn");
    signup_btn.onclick = function(e){
        var email = document.getElementById("sign-up-email").value;
        var pass = document.getElementById("sign-up-pass").value;
        addAccount(email,pass);
    };
    
    var login_btn = document.getElementById("login");
    login_btn.onclick = function(e){
        var email = document.getElementById("login-email").value;
        var pass = document.getElementById("login-pass").value;
        login(email,pass,successCallback,failedCallback);
    };
}
